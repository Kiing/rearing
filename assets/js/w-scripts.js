(function ($) {

	// scripts here
	$('.search-form').on('submit', function (e) {
		var search = $(this).find('input');
		if (search.val().length < 1) {
			e.preventDefault();
			search.trigger('focus');
		}
	});

	$(document).ready(function () {
		var lastScrollTop = 0;
		$(window).scroll(function (event) {
		  var currentScroll = $(this).scrollTop();
		  if ($(this).scrollTop() > lastScrollTop) {
			$('.menu-desktop').addClass('fixed');
		  } else {
			$('.menu-desktop').removeClass('fixed');
		  }
		});
	});

	$('.list-item').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		nextArrow: '.next',
		prevArrow: '.btn-slick',
		responsive: [{
				breakpoint: 768,
				settings: {
					arrows: false,
					centerPadding: '40px',
					slidesToShow: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	});

	$('.menu-mobile .icon').click(function () {
		$('.menu-mobile .list-menu').addClass('show');
		$('.overlay').addClass('show');
	});

	$('.overlay').click(function () {
		$(this).removeClass('show');
		$('.menu-mobile .list-menu').removeClass('show');
	});

	// $('.w-banner .play').on("click", function(){
	// 	$.fancybox({
	// 		herf: this.herf,
	// 		type: $(this).data("type")
	// 	});
	// 	return false
	// });

	$(document).ready(function () {
		// Fancybox.bind("[data-fancybox='gallery-video']", {
		// 	on: {
		// 		load: (fancybox, slide) => {
		// 			console.log(`#${slide.index} slide is loaded!`);
		// 			console.log(
		// 				`This slide is selected: ${fancybox.getSlide().index === slide.index}`
		// 			);
		// 		},
		// 	},
		// });
		Fancybox.bind('[data-fancybox="gallery-video"]', {
			Carousel: {
				on: {
					change: (that) => {
						mainCarousel.slideTo(mainCarousel.findPageForSlide(that.page), {
							friction: 0,
						});
					},
				},
			},
		});
	});
}(jQuery));